import psycopg2

# Connect to your postgres DB
conn = psycopg2.connect("dbname=dbs_project user=jonah")

# Open a cursor to perform database operations
cur = conn.cursor()

# Execute a query
cur.execute("SELECT TRUE")

# Retrieve query results
records = cur.fetchall()

print("Sucess!")
