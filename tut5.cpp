#include <sqlite3.h>
#include <stdio.h>

int main() {
    sqlite3 *db;
    sqlite3_stmt *query;

    int rc = sqlite3_open("test.sqlite", &db);
    rc = sqlite3_prepare_v2(db, "SELECT SQLITE_VERSION()", -1, &query, 0);
    rc = sqlite3_step(query);

    if (rc == SQLITE_ROW) {
        printf("%s\n", sqlite3_column_text(query, 0));
    }

    rc = sqlite3_prepare_v2(db, "SELECT 1", -1, &query, 0);
    rc = sqlite3_step(query);

    if (rc == SQLITE_ROW) {
        printf("%s\n", sqlite3_column_text(query, 0));
    }

    sqlite3_finalize(query);
    sqlite3_close(db);

    return 0;
}
