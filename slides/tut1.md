---
theme: "metropolis"
---

# 1. DBS Tutorium

## Motivation

- Praxisrelevant
    - Datenbanken kommen vor in
        - Mobilen Apps
        - Desktop Apps
        - Web Service Backends
- Abwechslungsreiches Themenfeld:
    - Theoretische Basis durch Relationale Algebra
    - Technischer Anteil
    - Projektplanungsanteil

## Übungszettel

- in Gruppen aus 3-4 Leuten **aus einem Tutorium von mir**
- Mindestens 70% der Punkte über alle Zettel
- Abgabe bitte nur als PDF

## Korrektur

- Feedback als Kommentare im PDF
- Bitte aktiv lesen, oft könnt ihr durch Erklärung noch mehr Punkte bekommen

## Aktive Teilnahme

- Mindestens 7 Tutorien müssen besucht werden
- Projekt muss bestanden werden

## Kennelernen

- Sucht eine Person die ihr noch nicht kennt, und redet mit ihr 2 Minuten lang über ein Thema
- Themen:
    - Haustiere
    - Reisen
    - wie kommt ihr zur Uni?
    - Warum belegst du dieses Modul / studierst deinen Studiengang?

## Übungsgruppen finden


## Kontakt

Jonah Brüchert

E-Mail: [jonahbeneb02@fu-berlin.de](mailto:jonahbeneb02@fu-berlin.de)

## Entity-Relationship-Modell

![](ER-Modell.jpg)

## Relationalionales Modell

```
Name(Spalte1, Spalte2, Spalte3)
```

* Primärschlüssel werden unterstrichen

## Beispiel: Bezahlsystem

Funktionen:

* Produkte
* Transaktionen
* Benutzer

Mit min-max Notation

## Beispiel: Videoplattform

* Videos
* ähnliche Inhalte
* Kommentare
* Accounts

## Vorstellen
