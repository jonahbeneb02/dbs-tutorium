# DBS Tutorium

Eine Sammlung von allen möglichen Dateien, die ich für mein DBS Tutorium erstellt habe.
Nicht alles davon ist notwendigerweise tatsächlich verwendet worden oder auf dem neusten Stand.

## Folien in PDF rendern

```bash
pandoc -f markdown -t beamer -o tut$i.pdf tut$i.md
```
