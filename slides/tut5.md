---
theme: "metropolis"
aspectratio: 169
header-includes:
    - \usepackage{listings}
---

# Verwendung von SQL

- Einzelnstehend: SQL Abfragen werden von der Anwender*in direkt in das Datenbanksystem eingegeben
- Eingebettet:
    - Statisch: Die möglichen Abfragen sind in einem Programm definiert
    - Dynamisch: Ein Programm generiert verschiedene Abfragen

# Eingebettetes SQL

- SQL-Abfragen werden aus einer anderen Programmiersprache abgesendet

Beispiel:
```Python
conn = sqlite3.connect('test.sqlite')
cursor = conn.execute("SELECT * from test")
```

# Vorteile (Auswahl)

- Datenbank kann zur Speicherung von Daten eines Programmes verwendet werden
- Ergebnisse können Domänenspezifisch dargestellt werden
- Variablen aus der Programmiersprache des Programmes können in SQL verwendet werden

# Vorteile von statischen Abfragen

- Effizienz
    - Abfragen müssen nur ein mal vom Datenbanksystem interpretiert und optimiert werden
    - Optimierte Abfrage kann wiederverwendet werden

# Sicherheit

- Anwender*innen des Programmes (z.B. einer Webseite) sollten keine beliebigen Abfragen ausführen können.

  Ungünstig wären z.B. `DROP TABLE accounts;` oder `SELECT * FROM payment_info`.

# Sicherheit

Beispiel:
```Python
conn.execute("INSERT INTO accounts (username) VALUES (" + username + ")")
  ```

Was passiert wenn jemand den Benutzernamen `"Katze123"); DROP TABLE accounts; --` wählt?

# Sicherheit

Zusammengesetzte Abfrage:
```SQL
INSERT INTO accounts (username) VALUES ("Katze123");
DROP TABLE accounts;
-- )
```

# Problem

- Das Datenbanksystem kann nicht zwischen Code und Werten unterscheiden
- alles wird ersteinmal als Code gelesen

# Parameterized queries

Verarbeitung mit Parameterized Queries / Prepared Statements:

1. Code wird interpretiert
1. Werte werden eingesetzt

$\Rightarrow$ Werte können nicht mehr als Code interpretiert werden, da sie erst eingesetzt werden nachdem das interpretieren Abgeschlossen ist.

# Parameterized queries

Beispiel in Python:
```Python
c.execute("INSERT INTO accounts (username) VALUES (?)", username)
```

# Übung

- Welche Programmiersprachen könnt ihr?

# Übung

- Bildet Gruppen
- Recherchiert und testet wie man sich in einer anderen Programmiersprache als Python mit einer Datenbank verbindet. Hier empfiehlt sich wieder SQLite, da ihr vermutlich gerade keinen Datenbankserver zur verfügung habt.

# Austausch

- Bildet neue Gruppen, so dass ihr in jeder neuen Gruppe Personen aus jeder vorherigen Gruppe habt
- Erklärt euch gegenseitig eure Ansätze
