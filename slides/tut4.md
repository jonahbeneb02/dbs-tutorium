---
theme: "metropolis"
aspectratio: 169
---

## Functional Dependencies

Die Werte von $X$ bestimmen die Werte eines anderen Attributs $Y$.

## Beispiel

\begin{tabular}{l|l|l|l}
ID & Name & Salary & Location \\
\hline
1 & Dana & 50000 & San Francisco \\
2 & Francis & 38000 & London \\
3 & Andrew & 25000 & Tokyo \\
\end{tabular}

## Beispiel

$\{A \rightarrow BD, B \rightarrow C, A \rightarrow D \}$

- Was ist die minimale Form?

## Armstrong Axiome

**Reflexivität**:
Y $\subseteq$ X, dann $X \rightarrow Y$

**Erweiterung**:
Wenn $X \rightarrow Y$ und Z Attribute, dann $XZ \rightarrow YZ$

**Transitivität**:
Wenn $X \rightarrow Y$ und $Y \rightarrow Z$, dann gilt auch $X \rightarrow Z$

## Weitere Regeln

**Vereinigung**:
Wenn $X \rightarrow Y$ und $X \rightarrow Z$, dann $X \rightarrow YZ$

**Zerlegung**: Wenn $X \rightarrow YZ$, dann $X \rightarrow Y$

**Pseudotransitivität**: Wenn $X \rightarrow Y$, und $ZY \rightarrow T$, dann $XZ \rightarrow T$

## Beispiel

$\{A \rightarrow BD, B \rightarrow C \}$

<!-- $\{A \rightarrow B, A \rightarrow D, B \rightarrow C, A \rightarrow C \}$ -->

- Welche funktionalen Abhängigkeiten können abgeleitet werden?

## Normalisierung

Warum?

- Speicherplatz sparen, für abhängige Attribute die sonst mehrmals den gleichen Wert annehmen würden
- Konsistenz, keine mehrmalige Aktualisierung von abhängigen Attributen
- Null-Values vermeiden
- Feinere Löschung von Informationen

## 1. Normalform

- Jede Spalte in einem Eintrag kann nur einen Wert enthalten

## 2. Normalform

- Das Schema erfüllt die erste Normalform
- Attribute müssen vom gesamten Schlüssel abhängen, nicht nur von einem Teil von ihm

  $\Rightarrow$ Schemata deren Schlüssel nur aus einem Attribut bestehen, sind automatisch in 2. NF

## 2. Normalform (Beispiel)

\begin{tabular}{l|l|l|l|l|l|l}
CD & Album & Interpret & Gründ. & Ersch. & Track & Titel \\
\hline
4711 & Not That Kind & Anastacia & 1999 & 2000 & 1 & Not That Kind  \\
4711 & Not That Kind & Anastacia & 1999 & 2000 & 2 & I'm Outta Love  \\
4711 & Not That Kind & Anastacia & 1999 & 2000 & 3 & Cowboys \& Kisses
 \\
4712 & Wish You Were Here & Pink Floyd & 1965 & 1975 & 1 & Shine On You
Crazy Diamond  \\
4713 & Freak of Nature & Anastacia & 1999 & 2001 & 1 & Paid my Dues  \\
\end{tabular}

- Welche Spalten bilden den Schlüssel?
- Welches Attribut hängt von was ab?

## 3. Normalform

- Das Schema erfüllt die zweite Normalform
- Attribute hängen nur vom Schlüssel ab. Nicht-Schlüssel-Attribute haben keine Abhängigkeiten untereinander.

## 3. Normalform (Beispiel)
\begin{tabular}{l|l|l|l|l|}
ID & Album & Interpret & Gründung & Erscheinung \\
\hline
4711 & Not That Kind & Anastacia & 1999 & 2000  \\
4712 & Wish You Were Here & Pink Floyd & 1965 & 1975  \\
4713 & Freak of Nature & Anastacia & 1999 & 2001  \\
\end{tabular}
