---
theme: "metropolis"
aspectratio: 169
header-includes:
    - \usepackage{listings}
---

## Evaluation der Lehrevaluation

## Online Analytical Processing

Expertengruppen

## Online Analytical Processing

* ROLAP:
    * Wird direkt auf Tabellen angewendet, ohne einen Würfel zu erzeugen
* MOLAP:
    * Arbeitet auf einem Multi-Dimensionalen Würfel
* HOLAP:
    * Kombiniert beides
    * Aus Tabellen erzeugte Data-Cubes werden zwischengespeichert.

## PostgreSQL einrichten

Auf das Benutzerkonto von PostgreSQL wechseln, und sich mit dem Server verbinden:
```bash
sudo -u postgres psql
```

Datenbank und Datenbankbenutzer erstellen
```SQL
create database dbs_project;
create user dbs;
grant all privileges on database dbs_project to dbs;
alter user dbs with password '1234';
```

Der Benutzer muss mit Standardeinstellungen von PostgreSQL genauso heißen wie der Benutzername auf dem Betriebssystem.

## Verbindung testen

```Python
import psycopg2

# Connect to your postgres DB
conn = psycopg2.connect("dbname=dbs_project user=<username>")

# Open a cursor to perform database operations
cur = conn.cursor()

# Execute a query
cur.execute("SELECT TRUE")

# Retrieve query results
records = cur.fetchall()

print("Sucess!")
```
