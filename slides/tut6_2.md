---
theme: "metropolis"
aspectratio: 169
header-includes:
    - \usepackage{listings}
---

## Attributhülle

$A^+$ ist Menge der Attribute die von $A$ abhängig sind.

## Minimale Hülle

- kleinstmögliche äquivalente Menge von Funktionalen Abhängigkeiten

## Erstellen der minimalen Hülle

1. **Dekomposition**: alle Funktionalen Abhängigkeiten die auf mehrere Attribute verweisen werden aufgeteilt.
1. **Unnötige Attribute auf der linken Seite** der Abhängigkeiten werden entfernt. Das sind alle Attribute die in der Attributhülle eines anderen Attributs auf der linken Seite vor kommen
1. Alle Abhängigkeiten die schon implizit durch **transitive Abhängigkeiten** definiert sind, werden entfernt.

## Zerlegung

Zerlegung soll verlustlos sein, und die Abhängigkeiten erhalten

## Zerlegung finden

- Schlüssel aus der Menge der Funktionalen Abhängigkeiten finden. Alle Abhängigkeiten müssen von ihm aus erreichbar sein.
- Eine neue Relation für jede Funktionale Abhängigkeit erstellen
- Wenn Schlüssel in einer Relation nicht enthalten ist, hinzufügen
- Wenn Relation danach in einer anderen Relation enthalten ist, die Relation entfernen
