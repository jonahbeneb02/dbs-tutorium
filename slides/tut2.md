---
theme: "metropolis"
header-includes:
    - \usepackage[normalem]{ulem}
---

## Beispiel: Getränke

Getränke:
\begin{tabular}{l|l}
    Name & Koffeingehalt in mg/100ml \\
    \hline
    Club Mate & 20 \\
    Mio Mio Mate & 20 \\
    Mio Mio Cola & 10 \\
\end{tabular}

Lager:
\begin{tabular}{l|l}
    Name & Menge (Kästen) \\
    \hline
    Club Mate & 5 \\
    Mio Mio Mate & 11 \\
    Mio Mio Cola & 20 \\
\end{tabular}

## Abfragen

 * Von welchen Getränken (Namen) sind weniger als 10 im Lager?
<!-- `SELECT Name FROM Lager WHERE Menge < 10`
 $\Pi_{Name}(\sigma_{Menge < 10}(Getränke))$-->
 * Von welchen Getränken (Namen) mit mehr als 10mg Koffein/100ml sind mehr als 10 Kästen vorhanden?
<!-- $\Pi_{Name}(\sigma_{Koffeingehalt > 10 \land Menge > 10}(Getränke \bowtie Lager))$
 `SELECT Name FROM Getränke NATURAL JOIN Lager WHERE Koffeingehalt > 10 AND Menge > 10`-->

## SQL als DDL

 * Erstelle die beiden Getränketabellen in SQL

<!--  `CREATE TABLE Getränke (Name TEXT PRIMARY KEY NOT NULL, Koffeingehalt INTEGER NOT NULL);` -->

<!--  `CREATE TABLE Lager (Name TEXT, Menge INTEGER NOT NULL, FOREIGN KEY (Name) REFERENCES Getränke(Name));` -->

## Erweiterung

Füge eine weitere Tabelle für Preise hinzu

## Beispiel

Wie könnten die Übungszettel-Funktionalität des Whiteboards in der Datenbank abgebildet sein?
Überlegt euch ein Entity-Relationship Modell, das mindestens die Folgenden Inhalte umfasst:

 * Assignments
 * Grades
 * Attachments
 * Submissions

Anschließend, erstellt die Tabellen in SQL

## Nachtrag

