---
theme: "metropolis"
aspectratio: 169
header-includes:
    - \usepackage{listings}
---


# Abfrageoptimierung
## Lehrevaluation

\centering
![](evaluation.png)

[lehrevaluation.fu-berlin.de/productive/de/sl/fT7L5K1gaChL](https://lehrevaluation.fu-berlin.de/productive/de/sl/fT7L5K1gaChL)

## Verarbeitung der Abfrage

1. Verstehen der Abfrage (parsing etc.)
2. Optimieren in relationaler Algebra
3. Erzeugen eines Ausführungsplans
4. Ausführen

## Regeln

- Unnötige Joins entfernen
- Join statt Kreuzprodukt verwenden
- Selektion soll so früh wie möglich statt finden (Priorität 1)
- Projektion soll früh stattfinden (Priorität 2)
- Verundete Selektionen nacheinander anwenden: $\sigma_{A \land B}(R) \rightarrow \sigma_{A}(\sigma_{B}(R))$

## Äquivalenzen

\begin{align*}
\sigma_P(R_1 \cup R_2) &= \sigma_P(R_1) \cup \sigma_p(R_2) \\
\sigma_P(R_1 - R_2)    &= \sigma_P(R_1) - \sigma_P(R_2) \\
(R_1 \cup R_2) \cup R_3 &= R_1 \cup (R_2 \cup R_3) \\
R_1 \cup R_2 &= R_2 \cup R_1
\end{align*}
