---
theme: "metropolis"
aspectratio: 169
header-includes:
    - \usepackage{listings}
---

# Speicherung

- Datenbank wird in mehreren Dateien gespeichert
- Jede Datei enthält eine Liste von Einträgen (Records)
- Jeder Eintrag besteht aus Feldern

# Fixed-length Records

- Einträge sind immer gleich lang
- Position eines Eintrags lässt sich berechnen ($length \cdot i$)

<!--# Löschung mit Fixed-length Records

Entweder:

- alle späteren Einträge verschieben
- letzten Eintrag in freien Platz verschieben
- nichts verschieben, sondern Eintrag nur für wiederverwendung vormerken-->

# Records mit variabler Länge mit Fixed-length Records

Entweder:

- Speicherung der eigentlichen Daten außerhalb der Tabelle, Verweis mit Pointer
- Platz rervieren, wenn maximale Länge bekannt ist

# Variable-lengh Records (Byte string representation)

- Einträge können unteschiedlich viel Platz verbrauchen
- werden mit Endzeichen beendet

# Variable-lengh Records (Pointer Method)

- Weitere Werte werden mittels Pointern an den Eintrag angehängt, als separater Eintrag mit nur diesem Feld

# Transaktionen

$\langle r_1(A), r_2(A) \rangle$ Kein Konflikt

$\langle w_1(A), w_2(A) \rangle$ Konflikt

$\langle w_1(A), r_2(A) \rangle$ Konflikt

# Konfliktgraph

- Knoten sind Transaktionen
- Wenn zwei Transaktionen $T_x, T_y$ im Konflikt stehen, zeichne Verbindung von zuerst auftauchender Transaktion ($T_x$) zu $T_y$
