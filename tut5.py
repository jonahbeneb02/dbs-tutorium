#!/usr/bin/env python3

import sqlite3

if __name__ == "__main__":
    conn = sqlite3.connect('test.py.sqlite')
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS test (id INTEGER PRIMARY KEY, str TEXT)")
    cur.execute("INSERT INTO test (str) VALUES (?)", ("test string",))
    cur.execute("INSERT INTO test (id, str) VALUES (?,?)", (3, "test string"))
    cursor = conn.execute("SELECT * from test")

    for row in cursor:
        print("id:", row[0])
        print("str:", row[1])
        print("-----")

    conn.close()
